import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import { colors } from "@material-ui/core";
import Logo from "../src/Assets/logo.png";
import img from "../src/Assets/dash.png";
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import {ThemeProvider } from '@material-ui/core/styles';
import {FormControl,Select,MenuItem,Menu} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: 275,
    minHeight: 688,
    borderRadius: 15,
    marginTop: 20,
    marginBottom: 25,
    marginLeft: 20,
    marginRight: 20,
    backgroundColor: "#AFADDE",
  },
  image:{
    width: '70px',
    height: '70px',
    fontSize: '1rem',
    marginLeft : 20,
    marginTop :10
  },
  logo:{
    width: '350px',
    height: '350px',
    fontSize: '1rem',
    marginLeft : 200,
    marginTop : 50
  },
  paperL:{
    margin: theme.spacing(6, 1),
    marginLeft : 120,
    marginTop:20,
    borderRadius: 35,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  paper: {
    margin: theme.spacing(6, 1),
    borderRadius: 35,
    display: 'flex',
    flexDirection: 'column',
  
  },
  tulis:{
      fontFamily: [
        'Alibaba-PuHuiTi-Heavy'
      ].join(','),
      fontSize:40,
      color:'#2F2F2F',
      marginRight:'70%',
  },
  // components: {
  //   MuiCssBaseline: {
  //     styleOverrides: `
  //       @font-face {
  //         font-family: 'Anton-Regular';
  //         font-style: normal;
  //         font-display: swap;
  //         // font-weight: 400;
  //         src: local('Anton-Regular'), url(${tulis}) format('ttf');
  //         unicodeRange: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF;
  //       }
  //     `,
  //   },
  // },
  tulis2:{
    fontFamily: [
      'Alibaba-PuHuiTi-Heavy'
    ].join(','),
    fontSize:40,
    color:'#6358DC',
    marginRight:'10%',
    
  },
  form: {
    width: '80%', // Fix IE 11 issue.
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),
    marginLeft:'10px',
  },
  submit: {
    // margin: theme.spacing(2, 0),
    marginTop:'10%',
    fontFamily: [
      'Alibaba-PuHuiTi-Heavy'
    ].join(','),
    fontSize:20,
    color:"#393F5F",
    background:"#BFD2F8",
    boxShadow: "440px  rgba(0,0,0,0.75);",
  },
  drop: {
			borderRadius: "100px",
			// boxShadow: "440px 40px 5px 0px rgba(0,0,0,0.75);",
      marginLeft:'82%',
	},
	list: {
		padding: 0,
	},
  title:{
    marginBottom:'40%',
    fontFamily: [
      'Alibaba-PuHuiTi-Heavy'
    ].join(','),
    fontSize:40,
    color:"#393F5F",
    marginLeft:'20%',
  },
  title1:{
    marginBottom:'40%',
    fontFamily: [
      'Alibaba-PuHuiTi-Regular'
    ].join(','),
    fontSize:18,
    color:"#393F5F",
    marginLeft:'35%',
  },
  title2:{
    // marginBottom:'10%',
    fontFamily: [
      'ITC_Avant_Garde_Gothic_LT_Bold'
    ].join(','),
    fontSize:18,
    color:"white",
    marginLeft:'35%',
  }
}));

export default function SimpleCard() {
  const classes = useStyles();
	const myList = ["Nurse", "Doctor"];
	const selectedOne = "Nurse";

  return (
    <Card className={classes.root}>
    <img src={Logo} className={classes.image} />
    <Grid container>
      <Grid item xs={6}>
      
      <img src={img} className={classes.logo} />
      <label className={classes.title}>Selamat Datang di AWAXI</label>
      <div>
        <label className={classes.title1}>Masuk dan Monitoring Pasienmu disini</label>
      </div> 
      
    </Grid>
      <Grid className={classes.paperL} item xs={4} component={Paper}  >
        {/* <div className={classes.paper}> */}
          <form className={classes.form} noValidate>
            {/* <ThemeProvider theme={theme}>
            <Typography variant="h3">LOGIN</Typography>
          </ThemeProvider> */}
          <h1 className={classes.tulis} >
            LOGIN
          </h1>
          {/* <ThemeProvider theme={theme}>
            <Typography variant="h3">AWAXI ACCOUNT</Typography>
          </ThemeProvider> */}
          <h1 className={classes.tulis2}   >
            AWAXI ACCOUNT
          </h1>
            <TextField
              variant="outlined"
              // margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <div className={classes.drop}>
                <FormControl>
                  <Select
                    MenuProps={{ classes: { list: classes.list } }}
                    id="demo-simple-select-outlined"
                    value={selectedOne}>
                    {myList.map((ele, index) => (
                      <MenuItem key={ele} value={ele} style={{ background: "#BFD2F8" }}>
                        {ele}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </div>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              
              className={classes.submit} 
            >
              Sign In
            </Button>
            </form>
        {/* </div> */}
      </Grid>
    </Grid>
  </Card>
    
  );
  
}